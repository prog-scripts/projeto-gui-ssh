#!/bin/bash

TLOGIN=$(yad --center --title="SCP"                                     \
	     --width=400 --heigth=400                                   \
	     --text="Você Deseja fazer um login existente ou um novo login?" --text-align="center" \
	     --button="Novo Login":0 --button="Login Existente":1       \
             )
	opi=$?

	if [ $opi -eq 1 ]; then
		ACANT=$(yad --center --title="SCP"                      \
			    --width=400 --heigth=400                    \
			    --text="Ultimos logins: "                   \
			    --form                                      \
			    --field=":CB" $(cat historico-acessos.txt | tr "\n" "!")
		       )
	        ex=$?
                  if [ $ex -ne 0 ]; then
			  exit 1
		  fi
		user=$(echo "$ACANT" | cut -d "@" -f 1)
                ipuser=$(echo "$ACANT" | cut -d "@" -f 2 | tr -d "|")

		pass=$(yad --form --field="Digite a senha: ":H)
		ex=$?
		  if [ $ex -ne 0 ]; then
			  exit 1
		  fi

		senha=$(echo $pass | cut -d "|" -f 1)

	elif [ $opi -eq 0 ]; then
		LOGIN=$(yad --center --title="SCP"                                      \
            		    --width=400 --heigth=400                                    \
                            --text="Digite o nome de usuário e o IP da máquina remota a ser feita a operação" --text-align="center" \
                            --form                                                      \
                            --field="Usuário:  " ""                                     \
                            --field="IP:  " ""                                          \
                            --field="Senha:  ":H                                        \
       	               )

		user=$(echo "$LOGIN" | cut -d "|" -f 1)
        	ipuser=$(echo "$LOGIN" | cut -d "|" -f 2)
        	senha=$(echo "$LOGIN" | cut -d "|" -f 3)

		echo "$user@$ipuser" >> historico-acessos.txt

		ex=$?
		if [ $ex -ne 0 ]; then
			exit 1
		fi

	else
		exit 1
	fi

MINICIAL=$(
        yad --center --title="SCP"                          \
	    --width=400 --heigth=400                        \
	    --text="Bem vindo ao SCP!" --text-align="center"\
	    --form                                          \
            --field="Escolha a operação desejada: ":CB "Upload!Download"\
            )

	ex=$?
	if [ $ex -ne 0 ]; then
		exit 1
	fi

op=$(echo "$MINICIAL" | cut -d "|" -f 1)


if [ $op == "Upload" ]; then
	ARQ=$(
   	   yad --center --title="SCP"                                       \
       	       --width=400 --heigth=400                                     \
       	       --text="Escolha o arquivo para fazer upload" --text-align="center"             \
       	       --file --multiple                                            \
       	       )

	ex=$?
	if [ $ex -ne 0 ]; then
		exit 1
	fi


	arquivo=$(echo "$ARQ" | tr -s "|" " ")
	if [ $? -eq 0 ]; then
		drem="/"
	else
		exit 1
	fi

	while true; do
		sshpass -p $senha ssh $user@$ipuser "ls -l -p -a '$drem' | grep '^d' | awk '{print \$NF}'" > /tmp/diretorios_remotos.txt
		sdir=$(yad --list --title="$user@$ipuser" --dual --text-align="center" --text="Diretório: $drem" --column="Diretórios" --separator='\n' --width=400 --height=400 --print-column=1 < /tmp/diretorios_remotos.txt)
		ex=$?
                if [ $ex -ne 0  ]; then
                         exit 1
                fi

		if [ $? -eq 0 ] && [ -n "$sdir" ]; then
			drem="$drem$sdir"
		else
			echo "" > /tmp/diretorios-remotos.txt
			break
		fi
	done

	sshpass -p $senha scp $arquivo $user@$ipuser:$drem
fi

if [ $op == "Download" ]; then
	DIR=$(
           yad --center --title="SCP"                                       \
               --width=400 --heigth=400                                     \
               --text="Escolha o diretório a receber o arquivo" --text-align="center"  \
               --file			                                    \
	       --directory                                                  \
               )

	  ex=$?

	  if [ $ex -ne 0 ]; then
		  exit 1
	  fi

	if [ $? -eq 0 ]; then
                drem="/"
        else
                exit 1
	fi

	while true; do
                sshpass -p $senha ssh $user@$ipuser "ls -p '$drem' | tr ' ' '\n' " > /tmp/arquivos_remotos.txt
		sarq=$(yad --list --title="$user@$ipuser" --text="Diretório: $drem" --text-align="center" --column="Diretórios" --separator='\n' --width=400 --height=400 --print-column=1 < /tmp/arquivos_remotos.txt)
		ex=$?
		if [ $ex -ne 0  ]; then
                         exit 1
                fi
                if [ $? -eq 0 ] && [ -n "$sarq" ] && [[ $sarq =~ /$ ]]; then
                        drem="$drem$sarq"
                else
			drem="$drem$sarq"
			echo "" > /tmp/arquivos-remotos.txt
                        break
                fi
		ex=$?
        done
        diretorio=$(echo "$DIR" | cut -d "|" -f 1)
	echo "$drem"
       	sshpass -p $senha scp $user@$ipuser:$drem $diretorio
fi
